# TP1 - PERRIN - RESEAU

# I. Exploration locale en solo
## 1. Affichage d'informations sur la pile TCP/IP locale

   Tout d'abord, affichons les infos des cartes réseau de notre PC
    
> cmd
> - ipconfig
```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::198b:2a67:c537:1594%20
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.243
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   ```
  ```
Carte Ethernet VirtualBox Host-Only Network :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::1d12:40c3:e65a:eab%19
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.56.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . :
```

   Affichons maintenant notre gateway
   
> cmd
> - ipconfig

```Passerelle par défaut. . . . . . . . . : 10.33.3.253```

En graphique maintenant, trouvons comment afficher les informations sur une carte IP
    
> Clic droit sur l'icône réseaux
> "Ouvrir les paramètres réseaux et internet"
> Ouvrir "Afficher les propriétés du matériel de la connexion"

![](https://i.imgur.com/SiKxzdw.png)


    A quoi sert la gateway dans le réseau d'YNOV ?

La gateway dans le réseau YNOV sert a relier les différentes machines du réseau à internet.

## 2. Modification des informations

###    A. Modification d'adresse IP (part 1)

Utilisons l'interface graphique de notre OS pour changer d'adresse IP :

> Aller dans le "Centre Réseau et partage"
> "Modifier les paramètres de la carte"
> Selectioner la carte WiFi
> Aller dans les propriétés de "Protocole Internet version 4 (TCP/IPv4)"
> Selectioner "Utiliser l'adresse IP suivante"

![](https://i.imgur.com/6Y9fX0d.png)

>Les règles du réseau peuvent forcer une adresse IP à un adresse MAC et refuser la connection s'il detecte une IP différente pour une adresse MAC, de ce fait, il est possible que nous perdions l'accès à internet

###   B. Table ARP
   
   Explorons la table ARP
   
> - cmd "arp -a"

L'adresse MAC de ma passerelle réseau est la seule affichée comme "Dynamique"

   Et si on remplissait un peu la table ?
   
> - ping 10.33.1.76
> - ping 10.33.1.103
> - ping 10.33.2.8

```
10.33.1.76            e0-cc-f8-7a-4f-4a     dynamique
10.33.1.103           18-65-90-ce-f5-63     dynamique
10.33.2.8             a4-5e-60-ed-0b-27     dynamique
```
   
###   C. nmap
   
   Utilisons nmap pour scanner le réseau de notre carte WiFi et trouver une adresse IP libre
   
> - cmd
> - nmap -v -sn -n 10.33.0.0/22
> J'utilise ici "-v" pour afficher les hosts "down", et "-n" pour skip la résolution DNS inverse et gagner du temps sur le scan
> - arp -a

![](https://i.imgur.com/epi2nlo.png)

####   D. Modification d'adresse IP (part 2)

   Modifions de nouveau notre adresse IP vers une adresse IP que nous savons libre grâce à nmap
   
> - nmap -sn -n 10.33.0.0/22

```
nmap -sn -n 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 11:13 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.6
Host is up (0.0090s latency).
MAC Address: 84:5C:F3:80:32:07 (Intel Corporate)
Nmap scan report for 10.33.0.7
Host is up (0.41s latency).
MAC Address: 9C:BC:F0:B6:1B:ED (Xiaomi Communications)
...
...
Nmap scan report for 10.33.3.236
Host is up (0.057s latency).
MAC Address: 3E:02:9D:7D:30:8F (Unknown)
Nmap scan report for 10.33.3.239
Host is up (0.045s latency).
MAC Address: 38:F9:D3:AE:C3:A3 (Apple)
Nmap scan report for 10.33.3.249
Host is up (0.057s latency).
MAC Address: 58:96:1D:17:43:F5 (Intel Corporate)
Nmap scan report for 10.33.3.252
Host is up (0.0050s latency).
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.0090s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0060s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.3.243
Host is up.
Nmap done: 1024 IP addresses (108 hosts up) scanned in 23.98 seconds
```

Je choisi ici l'IP 10.33.3.243
    
> - ipconfig

```
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::198b:2a67:c537:1594%21
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.243
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253 
   ```

Ici on voit bien que l'IP choisi est reconnue par la commande ipconfig 
   
> - ping 8.8.8.8

```
Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=25 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=40 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=31 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 23ms, Maximum = 40ms, Moyenne = 29ms
```
Le ping au ``8.8.8.8`` renvoie une réponse, je suis donc conecté à internet

# II. Exploration locale en duo
##   1. Prérequis

> - deux PCs avec ports RJ45 - Check
> - un câble RJ45 - Check
> - firewalls désactivés sur les deux PCs - Check

On a tout ce qu'il nous faut !

##   2. Câblage
   
Avec un peu d'effort et de bonne volonté, nous avons réussi à brancher les deux PC l'un à l'autre à l'aide du câble RJ45

##   3. Modification de l'adresse IP


Nous avons choisi les adresse 192.168.10.1 pour moi, et 192.168.10.2 pour lui, avec pour masque 255.255.255.252, donc un /30.

```
Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . : home
   Adresse IPv6 de liaison locale. . . . .: fe80::41aa:b69d:f9f8:9722%17
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
   ```
   
   > - arp -a
   
   ```
   Interface : 192.168.10.1 --- 0x11
  Adresse Internet      Adresse physique      Type
  192.168.10.2          3c-7c-3f-1a-6c-49     dynamique
  ```
   
   De mon côté j'execute la commande
   > - ping 192.168.10.2
   
   ```
Envoi d’une requête 'Ping'  192.168.10.2 avec 32 octets de données :
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
```
   Du sien il execute :
   > - ping 192.168.10.1
```
Envoi d’une requête 'Ping'  192.168.10.1 avec 32 octets de données :
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.10.1 : octets=32 temps=2 ms TTL=128
```
Ici nous pouvon donc observer que les deux machines peuvent communiquer entre elles par le biais du ```ping```

 ##  4. Utilisation d'un des deux comme gateway

 
   Pour tester la connectivité à internet nous allons effectuer des requêtes simples vers des serveurs connus

```
PS C:\Users\Muralee> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 20ms, Maximum = 22ms, Moyenne = 21ms
 

```

Ici Mukesh à bien réussi à ping l'ip de Google, il est donc bien connecté à Internet via mon PC

Utilisons maintenant un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)

```
PS C:\Users\Muralee> tracert 192.168.10.1

Détermination de l’itinéraire vers LAPTOP-FLEOPDHT [192.168.10.1]
avec un maximum de 30 sauts :

  1     1 ms     1 ms     1 ms  LAPTOP-FLEOPDHT [192.168.10.1]

Itinéraire déterminé.
PS C:\Users\Muralee> tracert 1.1.1.1

Détermination de l’itinéraire vers 1.1.1.1 avec un maximum de 30 sauts.

  1     1 ms     *        2 ms  LAPTOP-FLEOPDHT [192.168.10.1]
  2     *        *        *     Délai d’attente de la demande dépassé.
  3     8 ms    12 ms     5 ms  10.33.3.253
  4     8 ms     5 ms     4 ms  10.33.10.254
  5    14 ms    10 ms    10 ms  92.103.174.137
  6    12 ms    11 ms    11 ms  92.103.120.182
  7    24 ms    22 ms    21 ms  172.19.130.117
  8    25 ms    24 ms    24 ms  46.218.128.74
  9    48 ms    22 ms    22 ms  195.42.144.143
 10    22 ms    19 ms    19 ms  1.1.1.1

Itinéraire déterminé.

```

##   5. Petit chat privé


Ci joint, un petit chat privé
![Alt Text](https://media.giphy.com/media/3nbxypT20Ulmo/giphy.gif?cid=ecf05e47aojgjgsuxz3qbl046it7m94nrux1zc5s0ctdxcoa&rid=giphy.gif&ct=g)
Bon. Trêve de plaisanteries.

    On utilise NetCat afin de pouvoir communiquer en local
    De mon côté j'execute la commande :

> - nc.exe -l -p 8888

    Du sien, il execute la commande :
    
> - nc.exe 192.168.10.1 8888

    Le résultat est le suivant :
    
![](https://i.imgur.com/IXAoSD1.png)



##   6. Firewall
   
Afin de prouver que le firewall est bien activé je vais executer la commande suivante :
    
> - netsh advfirewall show allprofiles state
    
```

Paramètres Profil de domaine :
----------------------------------------------------------------------
État                                  Actif

Paramètres Profil privé :
----------------------------------------------------------------------
État                                  Actif

Paramètres Profil public :
----------------------------------------------------------------------
État                                  Actif
Ok.
```

Très bien, le firewall est donc activé. Je vais maintenant accepter les demandes d'Echo ICMPv4 dans les règles de mon firewall.
    
![](https://i.imgur.com/cRzUc1q.png)

Maintenant, je vais créer une nouvelle règle afin d'autoriser tout trafic entrant du port 8888
Je t'épargnes les screens interminables, alors en voilà juste un montrant que la règle existe :
    
![](https://i.imgur.com/r5VgVMT.png)

Maintenant que ceci est fait, on va pouvoir se reconnecter sur NetCat, firewall activé, et tester tout ça !

![](https://i.imgur.com/RRvGYiw.png)
Ca fonctionne !

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP 
   
   
Afin de découvrir mon bail DHCP , je vais éxecuter la commande suivante :
      
> - ipconfig /all

```
Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 16:20:08
Bail expirant. . . . . . . . . . . . . : samedi 18 septembre 2021 16:20:05
```

On voit ici la date et l'heure précise à laquelle j'ai obtenu mon bail, et celle ou il expirera

## 2. DNS

Pour trouver l'adresse IP du server DNS que connaît mon ordinateur, je vais également utiliser la commande :
    
> - ipconfig /all

```
 Serveurs DNS. . .  . . . . . . . . . . : 2001:861:80:7780:12d7:b0ff:feca:b3bc
                                          192.168.1.254
```

Nous allons maintenant executer la commande 'nslookup' pour faire des requêtes DNS manuellement
    
> - google.com


```
Serveur :   bbox.lan
Address:  2001:861:80:7780:12d7:b0ff:feca:b3bc

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4006:801::200e
          172.217.18.46
```

> - ynov.com

```
Serveur :   bbox.lan
Address:  2001:861:80:7780:12d7:b0ff:feca:b3bc

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

    Ici on peut voir l'origine de la demande (ma box), et le serveur DNS de l'adresse recherchée (pour Ynov.com : 92.243.16.143)
    



# IV. Wireshark
   
   
  