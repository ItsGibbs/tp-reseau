# TP2 - PERRIN - RESEAU

# I. ARP

## 1. Échange ARP

#### 🌞Générer des requêtes ARP

On va commencer par ```ping``` entre les deux VM
```
[admin@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.225 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.405 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.560 ms
```

Observons maintenant les tables arp des deux machines avec ```arp -a```
```
[admin@node1 ~]$ arp -a
? (10.2.1.12) at 08:00:27:f5:59:0c [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:07 [ether] on enp0s8 
```
```
[admin@node2 ~]$ arp -a
? (10.2.1.11) at 08:00:27:47:99:b6 [ether] on enp0s8
? (10.2.1.1) at 0a:00:27:00:00:07 [ether] on enp0s8 
```
On peut ici ressortir les adresses MAC des deux machines, respectivement ```08:00:27:47:99:b6``` pour ```node1``` et ```08:00:27:f5:59:0c``` pour ```node2```

On peut vérifier que l'adresse MAC de ```node2``` récupérée depuis ```node1``` est la bonne en executant la commande ```ip a``` depuis ```node2```
```
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f5:59:0c brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
     [...]
```
On voit que l'adresse MAC liée à notre Host-Only (``enp0s8``) est bien la même qu'affichée sur notre arp lancé depuis ``node1``

## 2. Analyse de trames

#### 🌞Analyse de trames

On va commencer par executer la commande ``$ tcpdump -i enp0s8 -s 65535 -w /home/tp2_arp.pcap`` afin de réaliser une capture de trame et l'enregistrer pour l'analyser plus tard.

On va ensuite viter notre table arp sur les deux machines à l'aide de la commande ``ip -s -s neigh flush all``

Sur ``node2`` par exemple, ça donne : 

```
[admin@node2 ~]$ sudo ip -s -s neigh flush all
10.2.1.11 dev enp0s8 lladdr 08:00:27:47:99:b6 used 2524/2521/2473 probes 1 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:07 ref 1 used 0/0/3 probes 1 DELAY

*** Round 1, deleting 2 entries ***
*** Flush is complete after 1 round ***
```

On va maintenant effectuer un ``ping`` entre les deux machines pour que les machines effectuent une requête ARP.

Une fois fait, on va pouvoir récupérer notre fichier ``tp2_arp.pcap`` et l'ouvrir avec Wireshark.
On isole les protocoles ARP et voici ce qu'on observe : 

![](https://i.imgur.com/IHnzuAc.png)

| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 5     | Requête ARP | `node1` `08:00:27:47:99:b6` | Broadcast `FF:FF:FF:FF:FF` |
| 208     | Requête ARP | `node1` `08:00:27:47:99:b6` | Broadcast `FF:FF:FF:FF:FF` |
| 209   | Réponse ARP | `node2` `08:00:27:f5:59:0c` | `node1` `08:00:27:47:99:b6` |
| 216   | Requête ARP | `node1` `08:00:27:47:99:b6` | Broadcast `FF:FF:FF:FF:FF` |
| 217   | Réponse ARP | `node2` `08:00:27:f5:59:0c` | `node1` `08:00:27:47:99:b6` |
    
# II. Routage

## 1. Mise en place du routage

#### 🌞Activer le routage sur le noeud ``router.net2.tp2``

```
[admin@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
#### 🌞Ajouter les routes statiques nécessaires pour que ``node1.net1.tp2`` et ``marcel.net2.tp2`` puissent se ``ping``
```
[admin@node1 /]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[admin@node1 /]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.501 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.89 ms
2 packets transmitted, 2 received, 0% packet loss, time 1038ms
```

```
[admin@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[admin@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.749 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.07 ms
[...]
2 packets transmitted, 2 received, 0% packet loss, time 1058ms
```
## 2. Analyse de trames

#### 🌞Analyse des échanges ARP

Après avoir clear la table ARP et envoyé un ``ping`` de ``Node1`` à ``Marcel``, en affichant à nouveau la table ARP sur les 3 noeuds on se rend compte que ``Node1`` et ``Marcel`` n'ont été en communication directe qu'avec Router.
En schéma, ça ressemble à ça : 

![](https://i.imgur.com/wxHq9Te.png)
###### *Note: Node2 est représenté dans ce schéma mais n'est pas activement utilisé pour cet exercice*

``Node1`` veut envoyer un ``ping`` à ``Marcel`` en indiquant comme chemin à emprunter ``10.2.2.254``. ``Router`` va donc passer par ``10.2.2.254`` et chercher ``Marcel``. Il lui envoie le ping avec comme provenance ``10.2.1.11``. ``Marcel`` répond donc par un ``pong`` à ``Node1`` en passant par ``Router`` à l'adresse ``10.2.2.254`` avec pour chemin ``10.2.1.254``. ``Router`` va donc rediriger le ``pong`` vers ``10.2.1.254`` pour le passer à ``Node1`` à l'adresse ``10.2.1.11``.

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | 10.2.1.11 | `node1` `08:00:27:47:99:b6` | 10.2.1.254 | Broadcast `FF:FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | 10.2.1.254 | `router` `08:00:27:18:ab:82` | 10.2.1.11 | `node1` `08:00:27:47:99:b6` |
| 3     | Ping        | 10.2.1.11 | `node1` `08:00:27:47:99:b6` | 10.2.2.12 | `router` `08:00:27:18:ab:82` |
| 4     | Requête ARP | 10.2.2.254 | `routeur` `08:00:27:58:6b:4e` | 10.2.2.12 | Broadcast `FF:FF:FF:FF:FF:FF` |
| 5     | Réponse ARP | 10.2.2.12 | `marcel` `08:00:27:61:77:82` | 10.2.2.254 | `router` `08:00:27:58:6b:4e` |
| 6     | Ping        | 10.2.2.254 | `router` `08:00:27:58:6b:4e` | 10.2.2.12 | `marcel` `08:00:27:61:77:82` |
| 7     | Pong        | 10.2.2.12 | `marcel` `08:00:27:61:77:82` | 10.2.1.11 | `router` `08:00:27:58:6b:4e` |
| 8     | Pong        | 10.2.1.254 | `router` `08:00:27:18:ab:82` | 10.2.1.11 | `node1` `08:00:27:47:99:b6` |

## 3. Accès internet

#### 🌞Donnez un accès internet à vos machines

- Ajoutez une route par défaut à node1.net1.tp2 et marcel.net2.tp2

```
[admin@node1 /]$ sudo ip route add default via 10.2.1.254 dev enp0s8
``` 

```
[admin@node1 /]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=15.8 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=16.9 ms
[...]
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
```

- Donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser


```
[admin@node1 /]$ sudo cat etc/resolv.conf
# Generated by NetworkManager
nameserver 1.1.1.1
```

```
[admin@node1 /]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 47619
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             224     IN      A       142.250.178.142

;; Query time: 15 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 29 17:17:21 CEST 2021
;; MSG SIZE  rcvd: 55
```

```
[admin@node1 /]$ ping google.com
PING google.com (172.217.19.238) 56(84) bytes of data.
64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=1 ttl=112 time=15.0 ms
64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=2 ttl=112 time=16.9 ms
64 bytes from par21s11-in-f14.1e100.net (172.217.19.238): icmp_seq=3 ttl=112 time=17.5 ms
[...]
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
```

#### 🌞Analyse de trames

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |
|-------|------------|---------------------|--------------------------|----------------|-----------------|
| 1     | Ping       | `node1` `10.2.1.12` | `node1` `08:00:27:47:99:b6` | `8.8.8.8` | `router` `08:00:27:18:ab:82` |
| 2     | Pong       | `8.8.8.8`  | `router` `08:00:27:18:ab:82` | `10.2.1.11` | `node1` `08:00:27:47:99:b6` |


# III. DHCP

## 1. Mise en place du serveur DHCP

#### 🌞Sur la machine node1.net1.tp2, vous installerez et configurerez un serveur DHCP

```
[admin@node1 /]$ sudo cat etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
default-lease-time 600;
max-lease-time 10800;
authoritative;
subnet 10.2.1.0 netmask 255.255.255.0 {
  range 10.2.1.12 10.2.1.253;
  option routers 10.2.1.254;
  option subnet-mask 255.255.255.0;
  option domain-name-servers 1.1.1.1;
}
```

```
[admin@node2 /]$ sudo cat etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for admin:
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=9580fc6e-a2f1-455a-b6de-0cc4f2e943f3
DEVICE=enp0s8
ONBOOT=yes

NETMASK=255.255.255.0
```
```
[admin@node2 /]$ ip a                                                                                                  M-A Mark Text    M-] To Bracket
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000                            M-6 Copy Text    M-W WhereIs Next
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f5:59:0c brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 393sec preferred_lft 393sec
    inet6 fe80::a00:27ff:fef5:590c/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
#### 🌞Améliorer la configuration du DHCP

```
[admin@node2 /]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.371 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.71 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1045ms
rtt min/avg/max/mdev = 0.371/1.038/1.706/0.668 ms
[admin@node2 /]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 100
[admin@node2 /]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=18.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=18.10 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 18.017/18.499/18.981/0.482 ms
[admin@node2 /]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45061
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             106     IN      A       142.250.75.238

;; Query time: 17 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Wed Sep 29 19:03:18 CEST 2021
;; MSG SIZE  rcvd: 55

[admin@node2 /]$ ping google.com
PING google.com (142.250.178.142) 56(84) bytes of data.
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=1 ttl=113 time=16.5 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=2 ttl=113 time=18.1 ms
64 bytes from par21s22-in-f14.1e100.net (142.250.178.142): icmp_seq=3 ttl=113 time=18.8 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 16.462/17.769/18.776/0.980 ms
[admin@node2 /]$
```
## 2. Analyse de trames

#### 🌞Analyse de trames