# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau

### 1. Adressage

🌞 **Vous me rendrez un 🗃️ tableau des réseaux 🗃️ qui rend compte des adresses choisies, sous la forme** :

| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
|---------------|-------------------|---------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.1.0`        | `255.255.255.128` | 126   | `10.3.1.126`         | `10.3.1.127`      |
| `client1`     | `10.3.1.128`      | `255.255.255.192` | 62 | `10.3.1.190`         | `10.3.1.191`      |
| `server2`     | `10.3.1.192`      | `255.255.255.240` | 14 | `10.3.1.206`         | `10.3.1.207`      |

🌞 **Vous remplirez aussi** au fur et à mesure que vous avancez dans le TP, au fur et à mesure que vous créez des machines, **le 🗃️ tableau d'adressage 🗃️ suivant :**

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|--------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3` | `10.3.?.?/?`         | `10.3.?.?/?`         | `10.3.?.?/?`         | Carte NAT             |
| ...          | ...                  | ...                  | ...                  | `10.3.?.?/?`          |


### 2. Routeur

#### 🌞 Vous pouvez d'ores-et-déjà créer le routeur. Pour celui-ci, vous me prouverez que :

- il a bien une IP dans les 3 réseaux, l'IP que vous avez choisie comme IP de passerelle

```
[admin@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:55:41:44 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85202sec preferred_lft 85202sec
    inet6 fe80::a00:27ff:fe55:4144/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:81:f0:eb brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe81:f0eb/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ca:df:f7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feca:dff7/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:40:bb:89 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe40:bb89/64 scope link
       valid_lft forever preferred_lft forever
```

- il a un accès internet

```
[admin@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=24.6 ms
[...]
```

- il a de la résolution de noms

```
[admin@router ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 24962
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             150     IN      A       172.217.19.238

;; Query time: 42 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Tue Oct 26 11:47:14 CEST 2021
;; MSG SIZE  rcvd: 55
```

- il porte le nom ``router.tp3``

```
[admin@router ~]$ hostname
router.tp3
``` 

- n'oubliez pas d'activer le routage sur la machine

```
[admin@router ~]$ sudo firewall-cmd --info-zone=public
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```


## II. Services d'infra

### 1. Serveur DHCP

### 2. Serveur DNS

#### A. Our own DNS server
#### B. SETUP copain

### 3. Get deeper

#### A. DNS forwarder
#### B. On revient sur la conf du DHCP

# Entracte

## III. Services métier

### 1. Serveur Web

### 2. Partage de fichiers

#### A. L'introduction wola
#### B. Le setup wola

## IV. Un peu de théorie : TCP et UDP
## V. El final